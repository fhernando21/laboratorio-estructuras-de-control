import java.util.Scanner; 
public class Ejercicio3Lab3 {

    public static void main(String[] args) {
        
        int a;
        System.out.println("Digite el numero a calcular el factorial: ");
        Scanner teclado = new Scanner(System.in);
        a = teclado.nextInt();
        factorial(a);


    }
    public static int factorial(int num){
        int fact=1;
        for (int i=num; i>1; i--){
            fact = fact*i;
            System.out.println(i+"*"+(i-1)+"="+fact);
        } return fact;

    }
}