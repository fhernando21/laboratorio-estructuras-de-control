import java.util.Scanner;

public class Ejercicio2Lab3 {


    public static void main(String[] args) {
        int numero; 
        int prime; 
        System.out.println("Empiece a digitar numeros: ");
        Scanner teclado = new Scanner(System.in);

        do {
            numero = teclado.nextInt();
            prime = verificar(numero);
            
        } while (prime>1);
        System.out.println("El numero que acaba de digitar es primo.");
    }

    public static int verificar(int num){
        

        int i=num-1; int primo = 0;
        do {
            if (num%i==0){
                primo = 1;
            }
            else {
                
                i=i-1;
            
            }
        } while (i>=2);
        return primo;
    }

}